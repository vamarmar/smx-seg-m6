# M6 Seguretat Vallbona

Pàgina d'Apunts i **exercicis** del mòdul M6

**ATENCIÓ: La informació proporcionada té un caràcter educatiu i en cap cas se'n pot fer un ús delictiu o maliciós. Tot el que feu, és sota la vostra responsabilitat. MAI ho heu de fer amb un equip de producció (màquina física o de la feina).**

---

## Exemples de comades de Linux

- Exemples de comandes Linux i casos d'ús - http://bropages.org

## Interessant

http://www.hackerhighschool.org/lessons.html

## Imatges Windows per les pràctiques

- Màquines virtuals preinstal·lades win7, win81, win10 - https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/

- En Azure, disc ISO, Disc dur virtual VHD de Windows Server 2012 (descàrrega directa)
  - https://go.microsoft.com/fwlink/p/?linkid=2195172&clcid=0x409&culture=en-us&country=us
  - https://www.microsoft.com/es-es/evalcenter/evaluate-windows-server-2012-r2

- Versions d'avaluació de Microsoft - https://www.microsoft.com/es-es/evalcenter (obrir menu de dalt)
