# PRÀCTICA: GESTIÓ I FORMATAT DE PARTICIONS (II)
# PRÀCTICA: SISTEMES RAID (II)

> ATENCIÓ: Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, etc. han de ser el vostre per a que a les captures es pugui veure.

> Teniu l'exercici a fer al final d'aquest enunciat. Llegiu primer tot el document abans de començar la pràctica.

> Abans de començar feu un "snapshot" o instantània de la maquina virtual del windows i el linux. Això farà que l'estat actual de la màquina virtual es guardi i pugueu fer tots els canvis que vulgueu sense por de fer malbé alguna cosa. Poseu-li de nom "Abans de particionat". En acabar, haureu d'esborrar aquesta instantània per tornar a l'estat anterior.

> En general, heu de comprovar SEMPRE que els canvis de configuració que feu produeixen l'efecte desitjat amb el conjunt de proves necessari.

## Referències i ajuda

## Objectius

- L'objectiu d'aquesta pràctica és comprovar com es gestionen les particions per consola en Windows.
- També crear un sistema RAID en Linux.

## Introducció


### Gestió de particions des de consola Windows

- Captura les comandes que fas servir a la consola i com queda el disc al final del procés.

1. Obre la consola de comandes i entra a la utilitat de gestió de particions "diskpart".

2. Mira l'ajuda escrivint "help".

3. Fes un llistat dels discos (list). Selecciona el disc (select).

4. Crea tres particions primàries (posa etiquetes p1, p2 p3) i una extesa mb 2 particions lògiques a dintre.

5. Selecciona i esborra la primera partició creada ("list partition", "select partition=numero", "delete partition").

6. Comprova el resultat amb l'eina gràfica (refresca amb F5, si cal).

### RAID amb Linux

1. Crea un sistema RAID 1 amb Linux.
