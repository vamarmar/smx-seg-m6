# MITJANS D’EMMAGATZEMAMENT

![](imgs/dispositius-emmag-2fa8d8ef.png)

**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [MITJANS D’EMMAGATZEMAMENT](#mitjans-demmagatzemament)
	- [MITJANS D’EMMAGATZEMAMENT](#mitjans-demmagatzemament)
		- [CLASSIFICACIO](#classificacio)
			- [1. SEGONS EL MODE D’ACCÉS A LES DADES](#1-segons-el-mode-daccs-a-les-dades)
			- [2. SEGONS L’ACCÉS DES DE LA CPU ALS DISPOSITIUS](#2-segons-laccs-des-de-la-cpu-als-dispositius)
			- [3. SEGONS EL SUPORT QUE FAN SERVIR:](#3-segons-el-suport-que-fan-servir)
	- [EMMAGATZEMAMENT REMOT: SAN, NAS, CLOUD](#emmagatzemament-remot-san-nas-cloud)
		- [1. SAN: (Storage Area Network)](#1-san-storage-area-network)
		- [2. NAS: (Network Attached Storage)](#2-nas-network-attached-storage)
		- [CLOUD](#cloud)

<!-- /TOC -->
---

## MITJANS D’EMMAGATZEMAMENT

L’emmagatzemament es relaciona amb dos processos:
- **Lectura** de dades guardats per transferir-los a la memòria RAM.
- **Escriptura** o gravació de dades per més tard poder recuperar-les i usar-les.

### CLASSIFICACIO

El dispositius d'emmagatzemament es poden classificar atenent a diferents criteris:

---

#### 1. SEGONS EL MODE D’ACCÉS A LES DADES

**Accès seqüencial**: L’element de lectura del dispositiu ha de passar per la totalitat de les dades guardades prèviament a les dades que es vol accedir.

**Accès aleatori**: L’element de lectura accedeix directament a on es troba físicament la informació que es vol accedir.

![](imgs/dispositius-emmag-1e3df4e8.png)

---

#### 2. SEGONS L’ACCÉS DES DE LA CPU ALS DISPOSITIUS

La CPU és un dispositiu molt ràpid que ha d'accedir als dispositius d'emmagatzemament i això produeix lentitud.

![](imgs/dispositius-emmag-b97173fe.png)

**Dispositius primaris**: És la memòria principal (RAM) accessible directament per la CPU. En apagar es perd la informació.

**Dispositius secundaris**: Dispositius que es troben dintre l'equip però no accessibles directament per la CPU.
- En apagar, mantenen la informació.
- Ex: Disc dur intern, disc SSD intern.

**Dispositius terciaris**: Sistemes generalment més lents que els secundaris.
- Cal inserir un suport (cinta, cd, dvd, etc.) quan es necessiten, de vegades de manera automàtica.
- Es fan servir per guardar informació que es fa servir molt poc. Normalment, es copia la informació abans de fer-la servir a un dispositiu secundari.
- Ex: Lector de Bluray, Unitat de cintes, Robot de cintes.

**Dispositius off-line**: Dispositius secundaris o terciaris que es desconnecten en acabar, connectats a un port de l'equip com ara USB.
- Necessiten intervenció humana per tornar-los a tenir disponibles.
- Són transportables.
- Ex: Pen drive o disc extern USB.

**Emmagatzemament remot**: Els dispositius d'emmagatzemament resideixen en equips connectats via xarxa local o xarxa WAN.
- Ex: NAS, SAN, Cloud.

---

#### 3. SEGONS EL SUPORT QUE FAN SERVIR:

Segons el suport que es fa servir per a guardar la informació tenim:

**Memòries (semiconductors)**:
- Memòria RAM (volàtil)
- Memòria ROM (no volàtil)
- Memòria Flash (no volàtil).
	- De molt alta velocitat, però amb poca capacitat.
	- Les memòries flash cada dia augmenten de capacitat i baixen de preu.

**Suport magnètics**: Superfície de plàstic o  metall recoberta de material magnètic.
- Menys velocitat que les memòries de semiconductors però amb més capacitat de dades.
- Cal anar amb compte perquè els imants els fan malbé.
- Ex: disquets, disc durs, cintes.

**Suports òptics**: Utilitzen un raig làser sobre una superfície metàl·lica amb forats (0’s).
- Capacitat de dades mitja, velocitat d'accés mitja (més lent que els discs durs).
- Ex: CD, DVD, Bluray.

---

## EMMAGATZEMAMENT REMOT: SAN, NAS, CLOUD

Tenim diferents sistemes d'emmagatzemament remot:

### 1. SAN: (Storage Area Network)

Xarxa utilitzada per connectar servidors, arrays de discs i llibreries de suport. Basada en tecnologia fibre channel i iSCSI.

- El tipus de tràfic en una SAN és de baix nivell i és molt similar al de discs durs com ATA, SATA i SCSI.

![](imgs/20180913-091402.png)

---

### 2. NAS: (Network Attached Storage)

Tecnologia d’emmagatzemament dedicada a compartir la capacitat d’un Servidor amb ordinadors personals o servidors clients a través d’una xarxa (normalment TCP/IP).

- Utilitza un Sistema Operatiu optimitzat per donar accès amb protocols de compartició com ara **CIFS, SMB, NFS, FTP o TFTP**.

<img src="imgs/20180913-091503.png" width="800" align="center" />

Esquema de connexions en xarxa d'un NAS

<img src="imgs/dispositius-emmag-2e2212a1.png" width="300" align="center" />

NAS domèstic Western Digital

---

### CLOUD

Emmagatzemament al núvol també conegut com a disc dur virtual. És un espai en un servidor que utilitzem per guardar arxius.

- Aquesta informació és accessible mitjançant internet (o també una xarxa local, privada).

![](imgs/20180913-091533.png)

Emmagatzemament al núvol
