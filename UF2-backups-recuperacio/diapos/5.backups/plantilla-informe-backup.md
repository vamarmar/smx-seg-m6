# Plantilla informe backup

## Preguntes que ens fem inicialment:

## 1. Fer còpia:

- Quins tipus de dades anem a copiar? (particions, sistema, aplicacions, bases de dades, fitxers)
- Quina metodologia de còpia utilitzarem segons les dades? (clonació, imatge forense, còpia externa de fitxers, còpia interna com ara contingut de la BD, etc)
- Què copiem? i Què no copiem? (llistes d'inclusió i exclusió, per exemple: arxius temporals, arxius multimèdia no importants, etc)
- Amb quina freqüència copiem? (cada minut, hora, dia, setmana, mes...)
- En quin moment del dia copiem? (matí, tarda, nit, cap de setmana, etc.)
- Còpia manual o automàtica? (qui ho fa o quin sistema automàtic com ara scripts, crontab, etc)?
- Qui fa les còpies, les controla, les transporta? (llista d'implicats, registre de còpies i restauració. ACL pels accedir als fitxers per fer la còpia)
- Quant (GB) ocupen les dades que copiem? (tamany de cada porció)
- Quant ocupa (GB) el total de còpies que guardem? (tamany acumulat)
- Quant de temps disposem/necessitem per a fer la còpia? (estimació de velocitat de còpia)
- Quins suports fem servir? (dispositius d'emmagatzemament)
- Tenen data de caducitat els suports?
- Quan es restauri existiran els dispositius de lectura?
- On guardem físicament els suports? (dintre o fora el CPD, armari ignífug, quines ubicacions externes, etc)
- Cal xifrar les dades? (per dades personals MOLT recomanable)
- Cal comprimir les dades? (per optimitzar espai, optimitzar ample de banda si va per xarxa)
- Quin tipus de còpia fem? (completa, diferencial, incremental)
- Com sabem si s'ha fet bé la còpia? (càlcul de hash)
- Com es registra en documents les còpies fetes?
- Fins quan seran vàlides les còpies? (rotació dels fitxers o suports, històrics)

## 2. Restaurar còpia

- Quins tipus de dades anem a restaurar.
- Com sabem que el backup està bé abans de restaurar? (hash)
- Qui pot restaurar? (administradors de còpies, és un procés destructiu)
- On restaurem? (equip de producció, extern, màquina virtual)
- Com es restaura la còpia? (procediment explicat)
- Podem restaurar parcialment o ha de ser total? (catàleg de cintes, llistat de continguts, versions disponibles...)
- Quant trigarem a restaurar? (el menys possible, estimar, simular, practicar)

---

## Quines dades anem a preservar i quines no?

- Considerar que la seva pèrdua afectaria la continuïtat del negoci.
- Indicar quines dades cal ignorar en la còpia.
- Indicar el tipus de dades i la metodologia que seguirem segons el tipus de dades
  - clonat (bit a bit, per particions, per sectors, sistemes d'arxius).
  - còpia d'arxius (tenir en compte ACL's per que quan restaurem pot ser que no els respecti).
  - còpia interna (per ex. bolcat de bases de dades amb utilitats pròpies).

## Quina freqüència tenen els processos de còpia?

- Depèn de la freqüència de modificació. A més modificacions, més freqüència de còpia.
- Mínim normalment, una còpia al dia.
- Molt important.

## Quan es faran les còpies?

- **Finestra de backup**: Període de temps en que es fa la còpia.
- En fred: El sistema no s'està fent servir (per exemple a la nit) i per tant la còpia pot ser idèntica.
- En calent: El sistema està en marxa i podria haver problemes de consistència
  - Comprovar arxius blocats: Si l'arxiu està en ús i blocat, no es copia i s'intenta més tard.
  - Snapshot: Es fa una còpia instantània del sistema (ho ha de permetre) i es copia aquesta.
  - Còpia de BD en calent: Es guarda un snapshot i totes les operacions que s'hauran d'executar mentre es produeix la còpia de seguretat, i que es guardaran en acabar la còpia.

## Quin volum de dades anem a copiar? (en GBytes o unitats adients)

- Si és molt gran, valorar comprimir les dades en destí.
- Hi ha tipus de dades que no es poden comprimir i no val la pena perdre-hi temps.
- Cal preveure la capacitat dels suports de còpia i comprovar que hi ha espai per fer la còpia.

## Quins suports i dispositius d'emmagatzematge usem?

- Valorar diferents aspectes com:
  - Capacitat de dades.
  - Velocitat d'escriptura.
  - Velocitat d'accés (lectura).
  - Nombre de suports necessaris.
  - Disponibilitat:
    - Extraïbles (necessiten intervenció humana o robot mecànic).
    - Fixos

## On guardarem els suports d'emmagatzematge?

- Buscar un contenidor per a cintes de backup o altres suports.
  - Ignífug
  - Ubicat el més lluny de l'equip del que copiem les dades
    - fora de l’equip
    - fora del CPD
    - fora de l'edifici.

## Qui controla o s'encarrega de fer o guardar o transportar les còpies?

- Cal mantenir la Cadena de confiança.
- Cal un control de suports.
- Guardar les còpies amb accés restringit.
- Assegurar per contracte la confidencialitat si són empreses externes.

## Com comprovem que les còpies s'han fet bé i són idèntiques?

- Cal Assegurar còpia idèntica i fiable. Calculem hash de cada fitxer copiat i de la còpia completa.
- Dispositius i suports fiables: cal controlar la data de caducitat dels suports.

## Com es farà la restauració de les dades?

- Planificar la restauració de les còpies
- Redactar manuals de restauració.
- Formació dels tècnics per a fer restauració.
- Restaurar la còpia en suports independents dels de producció.
- Restaurar les dades en el menor temps possible.

## Com es farà les proves de restauració? Cada quant?

- Cal provar el sistema de restauració regularment.

## Fins quan seran vàlides les còpies?

- Establir si per normativa legal o normatives de l'empresa les dades emmagatzemades s'han de destruir.
- Establir mecanismes de destrucció de les dades que ja no són vàlides i dels suports obsolets.

## Segueixen sent vàlids els suports on hi ha les còpies?

- Cal vigilar l'**obsolescència de dispositius** d’emmagatzematge.
- Cal guardar dispositius antics si hi ha còpies històriques que es guarden molts anys.

## Les dades són especialment sensibles o volem assegurar confidencialitat?

- Normalment cal xifrar les dades en general i les personals en particular molt més per llei.

## Quin cost té el maquinari, programari i feina que suposa?

- Cal valorar quan comprem dispositius d'emmagatzematge.
- Cost del programari.
- També qui o què farà les còpies (manual o automàtic).
- Preparar scripts automàtics i programació de tasques.

## Quin Ample de banda de xarxa necessitem?

- Si es fa en dispositius de xarxa.
- Recomanable una xarxa dedicada per al dispositius de backup.

## Quins usuaris estan autoritzats per crear i restaurar?

- Configurarem ACL's i drets d'us per poder copiar i restaurar (no te per què ser el mateix qui copia i qui restaura).

## Com validem les còpies?

- La còpia es fa bé (Hash de les còpies, se segueixen les polítiques de còpia).
- La Política de backup és correcta (auditoria d'empreses externes).

## Quins registres portem?

- Cal crear informes que continguin:
    - Registre de còpies.
    - Registres d'accessos a dades.
    - Registre de canvis.
