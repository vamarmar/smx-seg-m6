# MESURES DE PROTECCIO DE LES DADES

![](img/1.proteccio-dades-4a712a48.png)

**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [MESURES DE PROTECCIO DE LES DADES](#mesures-de-proteccio-de-les-dades)
	- [DESASTRES](#desastres)
	- [QUE POT DESTRUIR LES DADES?](#que-pot-destruir-les-dades)
	- [MESURES DE SEGURETAT](#mesures-de-seguretat)
	- [RESPOSTES TIPIQUES A FALLADES DE MAQUINARI](#respostes-tipiques-a-fallades-de-maquinari)
		- [1. SUBSTITUIR](#1-substituir)
		- [2. DUPLICACIO (REDUNDANCIA): ALTA DISPONIBILITAT O HA (HIGH AVAILABILITY)](#2-duplicacio-redundancia-alta-disponibilitat-o-ha-high-availability)
	- [RESPOSTES TIPIQUES A FALLADES DE PROGRAMARI](#respostes-tipiques-a-fallades-de-programari)
	- [PLA DE CONTINGENCIA](#pla-de-contingencia)

<!-- /TOC -->

---

## DESASTRES

Per molt protegit que es tingui tot, en algun moment pot passar un desastre. Es poden fer malbé moltes coses:
- Maquinari
- Programari
- Dades

## QUE POT DESTRUIR LES DADES?

Les dades són **el més important** d’una organització
- La pèrdua de dades pot ser crítica
- Hi ha moltes amenaces a les dades:
  - Accidents
  - Malware
  - Atacs
  - Fallades de maquinari
  - Fallades de proveïdors (llum, telecomunicions, etc.)
  - etc.

---

## MESURES DE SEGURETAT

**1. Preventives (Actives)**: Aquestes mesures permeten anticipar un desastre per actuar abans que es produeixi. Per exemple, sistema SMART en discs durs.

![](imgs/1.proteccio-dades-02fcc7e0.png)

Captura de l'eina "discs" d'ubuntu.

![](imgs/1.proteccio-dades-0ef03033.png)

Inici de sistema amb disc en mal estat

- **2. Correctores o pal·liatives (Passives)**: Aquestes mesures busquen tornar a l'estat anterior al desastre el més aviat possible. Per exemple, els backups.

---

## RESPOSTES TIPIQUES A FALLADES DE MAQUINARI

1. Substituir components
2. Duplicar components (redundància)

### 1. SUBSTITUIR

Normalment això no sol ser tant fàcil com sembla i es necessiten una sèrie de passos:
1. Desembalar.
2. Instal·lar Sistema Operatiu.
3. Instal·lar drivers de dispositius.
4. Actualitzar Sistema Operatiu.
5. Instal·lar programari i configurar.

### 2. DUPLICACIO (REDUNDANCIA): ALTA DISPONIBILITAT O HA (HIGH AVAILABILITY)

Es tracta de que quan falla un element, un altre IGUAL continua funcionant.

A diferents nivells, podem duplicar maquinari:
- Fonts d'alimentació duplicades.
- Controladores de disc duplicades.
- **Channel bonding** (duplicar targetes de xarxa).
- Discs durs duplicats (per ex. **sistemes RAID**).
- Servidors complets duplicats (complex de fer).
- CPD complet (molt complex de fer).
- Mirroring per xarxa.
- Etc.

---

## RESPOSTES TIPIQUES A FALLADES DE PROGRAMARI

**Solució bàsica**: Reinstal·lar. Pot fer perdre molt temps.
- Cal solucionar els problemes el més aviat possible. Hi ha organitzacions que no es poden permetre estar molt de temps aturades.

**Restaurar una imatge** completa del sistema. És una solució preferible. Després segurament caldrà restaurar **dades d'aplicacions**.

---

## PLA DE CONTINGENCIA

No es poden prendre decisions en el moment del desastre, ja que tenim una emergència. Cal tenir fet un **pla de contingència**.

![](imgs/1.proteccio-dades-ae3e36bd.png)

Porcès per elaborar un pla de contingència

**Pla de contingència**: Document que conté la descripció de les passes a seguir per tornar a la normalitat després d'un incident de seguretat.
- Preveu els problemes abans que passin i determinar quins són els elements crítics i els no crítics
- Determina què fer en cas de fallada.
- Determina la política de còpies de seguretat.
- Determina les responsabilitats del personal.

Les empreses canvien i **cal anar revisant el pla de contingència**.
