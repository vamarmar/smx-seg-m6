# Organització de discos

![](imgs/organització-discos-2bff61ee.png)

**INS Carles Vallbona**

**Pau Tomé**

# Disc durs

![](imgs/organització-discos-bfd80a43.png)

## Descripció Bàsica

- **Disc dur (Hard Disk Drive o HDD)** és un dispositiu d'emmagatzemament no volàtil. S'hi guarden grans quantitats de dades digitals en la superfície magnetitzada dels diversos discs (platter) que conté, els quals giren a gran velocitat.

- Alguns dels principals termes per a calcular la capacitat d'un disc són:

    - **Plat**: Cadascun dels discos que hi ha dintre del disc dur.
    - **Cara**: Cadascun dels dos costats d'un plat
    - **Capçal**: El nombre de capçals; equival a donar el nombre de cares, ja que hi ha un capçal per cara.
    - **Pista**: Una circumferència dintre d'una cara; la pista 0 està situada a la vora exterior.
    - **Cilindre**: Conjunt de diverses pistes; són totes les circumferències que estan alineades verticalment (una de cada cara).
    - **Sector** : Cadascuna de les divisions d'una pista. És la unitat bàsica de treball. Sempre que s'accedeix a un disc, es llegeix/escriu mínim un sector sencer.
      La grandària del sector no és fixa, sent l'estàndard més habitual de 512 bytes. Antigament el nombre de sectors per pista era fix, la qual cosa desaprofitava l'espai significativament, ja que a les pistes exteriors es poden emmagatzemar més sectors que en les interiors.

    - **Temps d'accés**: S'obté de la suma de:
      1. Seek time = posicionament a la pista + temps d'espera del sector buscat.
      2. Temps de transferència, des del dispositiu a memòria RAM.

- Simulador de fallades de disc dur (versió flash) - https://drivesaversdatarecovery.com/company/hard-drive-crash-simulator/

## Particions

- Per guardar la informació als discos durs, es creen particions (divisions lògiques del disc, tractades com a discos diferents).

- Models de particionat:
  - **Particions MBR (Master Boot Record)**. Model Antic que funcionava amb sistema d'arrencada BIOS.
  - **GPT (GUID Partition Table)** (GUID és Global Unique Identifier). Model més modern i que s'hauria de fer servir en discs grans. Funciona amb sistema d'arrencada UEFI.

## Avantatges del particionat

- Poder usar sistemes d'arxius antics (ex: FAT), ja que tenen tamany màxim i cal dividir el disc en trossos més petits.
- Es pot guardar una còpia de restauració ràpida a una altra partició (ex: portàtils).
- Permeten coexistir diferents sistemes operatius i sistemes d'arxius diferents.
- Separar la partició d'intercanvi (swap).
- Guardar per separat la informació dels usuaris per si cal reinstal·lar el sistema operatiu en una partició diferent.
- El possible desbordament de dades d'usuari no afecta al sistema (quotes físiques).

## Particionat MBR

- BIOS (Basic Input-Output System), creat per IBM al 1983 és el primer programa (firmware) que s'executa en quan el sistema s'inicia. Sol estar emmagatzemat en una memòria flash ubicada a la placa base, independent del sistema d'emmagatzemament.

- **MBR**: Creat per Microsoft al 1983. És un registre al sector 0 del disc dur (512 bytes), llegit per la BIOS en l'arrencada. Conté només 4 entrades (1 per partició) per a indicar on comencen les particions al disc.

![](imgs/organització-discos-bb7c0232.png)
Organització de la MBR (CHS=Cilynder,Head,Sector)

![](imgs/organització-discos-453ca0fa.png)
Organització del disc
![](imgs/organització-discos-b2dd18b6.png)

## Procés d'arrencada del PC amb MBR

![https://www.incibe-cert.es/blog/bootkits](imgs/organització-discos-05dc3ec6.png)

1. El sistema s'engega i s'executa l'autotest (power-on self-test o POST)
2. BIOS inicialitza el maquinari requerit per arrencar (disc, controladors de teclat, etc.).
3. La BIOS llegeix el sector 0 del primer disc de la llista BIOS. Executa els primers 440 bytes (Codi d'arrencada de la Master Boot Record).
4. Aquest carregador de la primera fase arrenca el codi de la segona fase des d'una partició (la que estigui activa o bé la que escull el codi del MBR)
5. El boot loader de veritat és arrencat i aquest carrega ja el sistema operatiu (en cadena, per exemple quan hi ha arrencada múltiple i GRUB ha de cridar el boot loader de Windows o bé carregant el kernel del sistema).


## Estructura d'un disc dur

- Tipus de particions MBR
  - **Partició Primària**: En poden haver fins a 4 (només hi ha 4 entrades a la taula MBR), o fins a 3 primàries i 1 estesa. Suporta un sistema d'arxius.
  - **Partició Estesa**: Partició que cal subdividir en particions lògiques. No suporta sistema d'arxius directament.
  - **Partició Lògica**: Subdivisió de la partició estesa. Es poden crear les que vulguem a dintre. Suporta sist. Arxius directament.
  - **Partició Activa**: Estat d'una partició PRIMÀRIA (pot iniciar SOP en el sistema MBR, només hi havia una activa).

## MBR vs GPT

![](imgs/organització-discos-f55535c4.png)
https://www.softzone.es/2016/03/25/mbr-gpt-estos-dos-estilos-particiones-discos/

# Sistema UEFI

- **UEFI (Unified Extensible Firmware Interface)**, creat al 2003 per Intel i perfeccionat al 2008 per UEFI fòrum per substituir la BIOS i el MBR. Té suport per llegir tant taules de particions com sistemes de fitxers.
- Per compatibilitat, UEFI manté el **MBR (de protecció)** per evitar que programari antic destrueixi dades de GPT.
- UEFI no arrenca mai el codi del Master Boot Record (MBR), existeixi o no. En comptes d'això, **arrenca programari ubicat a la NVRAM** (de la placa mare del PC).
- UEFI suporta nativament sistemes d'arxius FAT12, FAT16, FAT32 i ISO-9660 (discs òptics).
- UEFI arrenca aplicacions EFI, o sigui, carregadors d'arrencada com per exemple GRUB (boot loaders), gestors d'arrencada (boot managers), shell UEFI, etc. Aquestes aplicacions estan guardades com a fitxers a la partició de sistema EFI (ESP=EFI System Partition). Cada fabricant pot guardar els seus fitxers a la ESP (partició de sistema EFI) a la carpeta "/EFI/nom_fabricant". Aquesta aplicació pot ser arrencada afegint una entrada d'inici a la NVRAM o des de la shell UEFI.

- Una **Shell UEFI** és un terminal pel firmware que permet arrencar aplicacions EFI que inclouen bootloaders UEFI. A més, el shell es pot usar per a obtenir informació sobre el sistema o el firmware. Per exemple, un mapa de memòria (memmap), canviar variables del boot manager (bcfg), executar programes de particionat (diskpart), carregar drivers UEFI, editar fitxer de text (edit), editar codi hexadecimal (hexedit), etc.

- L'especificació UEFI te suport per a **BIOS heredades (legacy)** arrencant amb el mòdul de suport de compatibilitat (**Compatibility Support Module o CSM**).

- Quan habilitem **CSM a la UEFI**, UEFI genera entrades d'arrencada per a tots els discos. Si escollim una entrada d'arrencada CSM, el sistema CSM de UEFI intentarà arrencar des del codi d'arrencada guardat al MBR, emulant un sistema BIOS.

https://wiki.archlinux.org/index.php/Arch_boot_process#UEFI

## Procés d'arrencada amb UEFI

![](imgs/organització-discos-192f11ca.png)

1. El sistema s'engega i s'executa l'autotest (power-on self-test o POST)
2. UEFI inicialitza el maquinari requerit per arrencar.
3. El Firmware llegeix les entrades d'arrencada de la NVRAM per determinar quina aplicació EFI arrencarà  i des de quin disc i partició.
4. Una entrada d'arrencada podria ser simplement un disc. En aquest cas, el firmware busca la partició de sistema EFI en aquell disc i intenta trobar una aplicació EFI a la ruta per defecte \EFI\BOOT\BOOTX64.EFI (BOOTIA32.EFI en sistemes de UEFI de 32-bit UEFI). Així s'arrenquen dispositius extraibles.
5. El Firmware arrenca l'aplicació EFI, que pot ser un carregador d'arrencada (boot loader) o el kernel d'un sistema operatiu. També podria ser una altra aplicació EFI com ara una shell EFI o un gestor d'arrencada (boot manager).
6. Si l'arrencada segura (Secure Boot) està activada, el procés d'arrencada verifica l'autenticitat de l'aplicació EFI amb la seva signatura digital.

- Un **carregador d'arrencada (boot loader)** es programari iniciat per la BIOS o UEFI. És responsable de carregar el kernel amb els seus paràmetres, i un disc RAM inicial (si és Linux) basat en els fitxers de configuració.

- El propi kernel pot ser arrencat directament per UEFI. Es pot encara usar un boot loader o boot manager per separat per editar els paràmetres del kernel abans d'arrencar.

# Particions GPT (GUID)

- **GPT (GUID Partition Table)**: nou estàndar que substitueix a MBR i està associat amb els sistemes UEFI.
- El seu nom  prové de que a cada partició se li associa un únic identificador global (GUID o anomenat UUID en Linux). És un identificador aleatori tan llarg que cada partició en el mon podria tenir el seu ID únic.

- Podem veure aquesta informació a Linux fent:
~~~
$ sudo blkid
...
/dev/sda1: UUID="99D7-651B" TYPE="vfat" PARTLABEL="EFI System Partition" PARTUUID="7766dada-5655-4fce-915a-daf6cf1088888"
/dev/sda3: UUID="76162250-a70f-4c50-9bf1-999f61d9194c" TYPE="crypto_LUKS" PARTUUID="29a5acd8-4632-4176-a53c-ba1ba5999999"
/dev/sda2: UUID="e60a2c63-2736-4198-9bb8-5c2dfc32f9d2" TYPE="ext4" PARTUUID="de1537af-3d57-4482-ab79-e584d99999999"
/dev/mapper/ubuntu--vg-swap_1: UUID="ec8a6cea-aad8-49ff-ab70-ca44b8888888" TYPE="swap"
...
~~~

- O bé:
~~~
$ sudo ls -l /dev/disk/by-uuid/
$ sudo ls -l /dev/disk/by-uuid/
total 0
lrwxrwxrwx 1 root root 10 de no 20 13:39 76162250-a70f-4c50-9bf1-999f61d9194c -> ../../sda3
lrwxrwxrwx 1 root root 10 de no 20 13:39 96855af9-494b-472d-aafd-f2f9e4bde2cc -> ../../dm-1
lrwxrwxrwx 1 root root 10 de no 20 13:39 99D7-651B -> ../../sda1
lrwxrwxrwx 1 root root 10 de no 20 13:39 e60a2c63-2736-4198-9bb8-5c2dfc32f9d2 -> ../../sda2
lrwxrwxrwx 1 root root 10 de no 20 13:39 ec8a6cea-aad8-49ff-ab70-ca44b8888888 -> ../../dm-2
~~~

- GPT no te cap límit (més enllà dels que tinguin els propis sistemes operatius:
    - Ni en tamany de les particions
    - Ni en el nombre de particions.
- Per exemple, Windows limita a 128 particions (taula de particions per defecte de GPT).

## Avantatges de GPT

- Cada partició te el seu propi **identificador de disc (GUID) i identificador únic de partició (PARTUUID)**. Així es pot referenciar el disc i la partició independentment del sistema d'arxius que contingui.
- Cada partició te la seva **etiqueta** independent del sistemes d'arxius (PARTLABEL).
- Podem tenir totes **les particions que vulguem** (limitat pel tamany de la taula de particions).
- No es necessiten particions esteses ni lògiques.
- Per defecte la taula GPT te espai per a 128 particions. Si es necessiten més particions, es pot reservar més espai per a taules de particions.
- Usa LBA (logical bloc address) de 64-bit per guardar números de sectors, de manera que el **tamany màxim de disc és de 2 ZiB** (2^30 GiB). En canvi MBR està limitat a 2 TiB d'espai per disc.

![](imgs/organització-discos-63b19961.png)

- GPT crea una **capçalera i una taula de particions de backup** al final del disc que permet recuperar la taula primaria en cas que es fes malbé.
- Usa **CRC32 per detectar errors** i corrupció de la capçalera o la taula de particions.

![](imgs/organització-discos-0683a25c.png)

https://es.wikipedia.org/wiki/Tabla_de_particiones_GUID

## Formatat d'una partició o disc

- Procès d'inicialització de l'espai del disc per poder guardar fitxers. Implica la creació d'un sistema de fitxers a dintre la partició.

- **Formatació de baix nivell o física**: inicialització dels blocs lògics en els plats del disc físic (crear pistes i sectors). Generalment es fa a la fàbrica i no acostuma a ser canviat.
- **Formatació d'alt nivell o lògica**: S'escriu les estructures del sistema de fitxers en determinats blocs lògics per fer que la resta dels blocs lògics estiguin disponibles per al sistema operatiu i les seves aplicacions. Es creen la MBR, les taules d'arxius i les taules de blocs lliures i ocupats (tot això ocupa espai del disc que no es pot usar pels usauris). Pot ser:
  - **Ràpid**: Només es declaren buits tots els blocs del disc. Les dades NO s'esborren (es podrien recuperar).
  - **Normal**: Es sobreescriuen tots els blocs de disc amb 0's.


- Per què es poden recuperar les dades? Com evitar-ho? - https://www.howtogeek.com/125521/htg-explains-why-deleted-files-can-be-recovered-and-how-you-can-prevent-it/

## Sistemes d'arxius

- **Sistema d'arxius**: component del sistema operatiu que controla com es guarden o recuperen dades als dispositius d'emmagatzemament.

- Funcions:
  - Gestió de l'espai de noms dels fitxers.
  - Gestionar l'espai d'emmagatzamament:
      - Assignar espai a arxius.
      - Alliberar espai d'arxius esborrats.
  - Administració de l'espai lliure/ocupat.
  - Accés a les dades guardades
      - Trobar/guardar les dades dels fitxers
  - Organitzar fitxers al sistema
  - Garantitzar proteccions de fitxers (ACL's)

## Tipus de sistemes d'arxius

- Cada sistema operatiu sol tenir el seu propi sistema d'arxius.

    - **FAT (16, 32)**: File allocation table (MSDOS, Windows 95, 98).
    - **NTFS**: New Technology File System (A partir de Windows NT, Windows 2000, XP, 7, 8, Server).
    - **Ext2, 3, 4**: Extended File System, Linux
    - **ReiserFS**: Linux
    - **HFS** (Hierarchical File System): MacOS
    - **HFS+** (Extended Hierarchical File System): MacOS

---

# Referències:

### Disc dur

- Disc dur - https://ca.wikipedia.org/wiki/Disc_dur
- Formatat de baix nivell - https://www.webopedia.com/TERM/L/LLF.html
- Formatat d'alt nivell - https://www.webopedia.com/TERM/H/HLF.html

### Eines de particionat

- Particionat - https://wiki.archlinux.org/index.php/Partitioning
- MBR amb fdisk - https://wiki.archlinux.org/index.php/Fdisk
- GPT (GUID) amb gdisk- https://wiki.archlinux.org/index.php/GPT_fdisk
- Eina gdisk (oficial) - https://www.rodsbooks.com/gdisk/

### Eines de recuperació d'arxius

- Llistat d'eines comunes - https://wiki.archlinux.org/index.php/File_recovery

### BIOS i UEFI

- BIOS vs UEFI (i MBR vs GPT): https://www.incibe-cert.es/blog/bootkits
- UEFI - https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface
- Recuperar arrencada UEFI des de Shell UEFI - https://www.frikisdeatar.com/recuperar-el-arranque-uefi-con-uefi-shell/
- Comandes de shell UEFI - https://docstore.mik.ua/manuals/hp-ux/en/5991-1247B/ch04s13.html
- UEFI Boot Manager (Intel) - https://software.intel.com/en-us/articles/uefi-boot-manager-1

###  Sistemes d'arxius

- Sistemes d'arxius - https://en.wikipedia.org/wiki/File_system
- FAT wikipedia - https://en.wikipedia.org/wiki/File_Allocation_Table
- FAT - http://www.ntfs.com/fat-systems.htm
- NTFS - https://en.wikipedia.org/wiki/NTFS
- NTFS tècnic - https://www.ntfs.com
- Ext3 - https://en.wikipedia.org/wiki/Ext3
- Ext4 - https://en.wikipedia.org/wiki/Ext4
- HFS - https://es.wikipedia.org/wiki/Sistema_de_archivos_de_Apple#Encriptaci%C3%B3n
